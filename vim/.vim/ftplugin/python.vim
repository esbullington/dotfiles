
let b:syntastic_python_python_exec = 'python3'
let g:syntastic_python_checkers=['flake8']

set expandtab
filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab
