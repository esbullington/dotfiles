shopt -s expand_aliases  
# Aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
# enable color support
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias ll='ls -alF --color=auto'
    alias la='ls -A --color=auto'
    alias l='ls -CF --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi
alias sb='source ~/.bashrc'
alias st='tmux source-file ~/.tmux.conf'
alias eb='vim ~/.bashrc'
alias ee='vim ~/.bash/.environment'
alias ea='vim ~/.bash/.bash_aliases'
alias es='vim ~/.bash/.secrets'
alias ev='vim ~/.vimrc'
alias et='vim ~/.tmux.conf'
alias exm='vim ~/.xmonad/xmonad.hs'
alias rxm='xmonad --recompile'
alias exd='vim ~/.Xdefaults'
alias sxd='xrdb ~/.Xdefaults'
alias gl='git status'
alias gp='git push'
alias xsync='xclip -o | xclip -selection clipboard -i'
alias blogdate='date +"%Y-%m-%d"'
alias grabit='tmux attach-session -d'
alias jserve='bundle exec jekyll serve'
alias makepassword='cat /dev/urandom | tr -dc "a-zA-Z0-9" | fold -w 32 | head -n 1'
alias makehash='cat /dev/urandom | tr -dc "a-zA-Z0-9" | fold -w 16 | head -n 1'
alias showrecipients=showGPGRecipients
alias clearhistory='history -c && history -w'
alias mitscheme='rlwrap -r -c -f "$HOME"/opt/mit-scheme/mit_scheme_bindings.txt scheme'
# `clipcopy' copies and `clippaste' pastes'
alias clipboard='xclip -sel clip'
alias xclipcopy='xclip -selection clipboard'
alias xclippaste='xclip -selection clipboard -o'
# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

